package main

import (
	"encoding/hex"
	"fmt"
	"github.com/bamchoh/pasori"
	"github.com/zserge/lorca"
	"log"
	"net/url"
	"time"
)

var html = `
<html>
<head>
	<meta charset="utf-8">
	<title>こんにちわーるど</title>
</head>
<body>
	<h1>こんにちわーるど！！</h1>
	<span id="idm"></span>
</body>
</html>
`

func main() {
	log.Println("こんにちわーるど")

	ui, err := lorca.New("", "", 480, 320)
	if err != nil {
		log.Fatalln(err)
	}
	defer ui.Close()

	_ = ui.Load("data:text/html," + url.PathEscape(html))

	// カードリーディング
	go func() {
		for {
			ui.Eval(fmt.Sprintf(`document.querySelector('#idm').innerText = '%s'`, "カードをかざしてください"))
			idm, err := pasori.GetID(0x054C, 0x06C3)
			if err != nil {
				log.Fatalln(err)
			}
			log.Println(idm)
			ui.Eval(fmt.Sprintf(`document.querySelector('#idm').innerText = '%s'`, hex.EncodeToString(idm)))
			time.Sleep(1 * time.Second)
		}
	}()

	<-ui.Done()
}
