module gitlab.com/tsuchinaga/lorca-tutorial

go 1.12

require (
	github.com/bamchoh/pasori v0.0.0-20190321133732-ca188cfb16ba // indirect
	github.com/lxn/win v0.0.0-20190716185335-d1d36f0e4f48 // indirect
	github.com/zserge/lorca v0.1.8 // indirect
	golang.org/x/sys v0.0.0-20190804053845-51ab0e2deafa // indirect
)
